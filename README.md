# Open Source Rust Rover

# First Steps
- [ ] Pick a name
- [ ] What features do we want (ie. Camera? Sensors? LoRa?)
- [ ] What MCU do we want? (Pico? Pi? SMT32?)
- [ ] Custom PCB?

## Feature ideas (WIP)
- Wheels (So I can be Ryan Gosling)
- Camera (Maybe Pi Camera?)
- Sensors
  - Temperature
  - Pressure
  - Look for cool sensors
- LoRa for remote radio control
- Command Center? 

# Basic Project Roadmap (WIP)
- [ ] Get electronics prototype made
- [ ] Get basic firmware working
- [ ] Finalize BOM
- [ ] Design rover in CAD 
- [ ] Design kicad PCB
- [ ] Program it
- [ ] Profit????
